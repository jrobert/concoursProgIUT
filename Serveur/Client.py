__author__ = 'julien'
from aiohttp import web
import asyncio
import json
import logging
from abc import ABCMeta, abstractmethod


class Client:
    __metaclass__ = ABCMeta

    def __init__(self):
        self.idJoueur = "spectateur"
        self.name = None

    def get_id_joueur(self):
        return self.idJoueur

    def set_id_joueur(self, id):
        self.idJoueur = id

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def __str__(self):
        print(self.idJoueur)
        print(self.name)
        return "Joueur n°" + str(self.idJoueur) + " name: " + self.name

    @abstractmethod
    def send(self, donnees): ...


class ClientWS(Client):
    def __init__(self, ws, controleur):
        super(ClientWS, self).__init__()
        self.ws = ws
        self.controleur = controleur

    def send(self, donnees):
        if not self.ws.closed:
            self.ws.send_json(donnees)

    def __hash__(self):
        return self.idJoueur

    async def recv(self):
        try:
            async for msg in self.ws:
                if msg.tp == web.MsgType.text:
                    logging.info("Got message %s" % msg.data)
                else:
                    return self.ws
            return self.ws
        finally:
            self.controleur.quitterJeu(self)

    def close(self):
        return self.ws.close()


class ClientTCP(Client, asyncio.Protocol):
    def __init__(self, controleur):
        super(ClientTCP, self).__init__()
        self.controleur = controleur
        self.idJoueur = None
        self.peername = None

    def connection_made(self, transport):
        self.peername, _ = transport.get_extra_info('peername')

        logging.info('Connection from {}'.format(self.peername))
        self.transport = transport
        self.controleur.onConnectTCP(self)

    def data_received(self, data):
        message = data.decode()
        logging.info('Recu de {} : {!r}'.format(self.name, message))

        if self.name:
            for message in message.strip().split('\n'):
                self.controleur.actionClient(self, message)
        else:
            res = json.loads(message.strip().split('\n')[0])
            if not "nickname" in res:
                # FAIRE ATTENDRE  ?? "
                self.transport.write(b"IL FAUT COMMENCER PAR CHOISIR UN NOM !")
            else:
                self.name = str(res["nickname"])

    def connection_lost(self, exc):
        self.controleur.quitterJeu(self)

    def send(self, donnees):
        self.transport.write(json.dumps(donnees).encode("utf8") + b"\n")

    def __str__(self):
        return super(ClientTCP, self).__str__() + " à l'IP " + self.peername

    async def close(self):
        pass
