from aiohttp import web
import asyncio
import logging
import json
import traceback
import argparse
import os.path
import sys

logging.basicConfig(level=20)

from Serveur import ClientWS, ClientTCP
from Jeu import Jeu, actions
DUREE_TOUR = 0.5

class Retours:
    def __init__(self):
        self.clear()

    def clear(self):
        self.retours_spectateur = []
        self.retours_clients = {}
        self.retours_tous = []

    def __repr__(self):
        return "spectateurs : " + str(self.retours_spectateur) + "clients : " + str(self.retours_clients) + "tous:" + str(self.retours_tous)

    def ajoute_tous(self, liste_actions):
        self.retours_tous += liste_actions

    def ajoute_spectateur(self, liste_actions):
        self.retours_spectateur += liste_actions

    def ajoute_joueur(self, id, liste_actions):
        self.retours_clients[id] = self.retours_clients.get(
            id, []) + liste_actions

    def get(self, id):
        if id == "spectateur":
            return self.retours_spectateur + self.retours_tous
        return self.retours_clients[id] + self.retours_tous

    def update(self, dico_retours):
        for (id, retours) in dico_retours.items():
            if id != "tous":
                self.ajoute_joueur(id, retours)
        self.ajoute_tous(dico_retours["tous"])


class Controleur:
    def __init__(self, **kwargs):
        self.clients = []
        self.actions = {}  # idjoueur -> action
        self.coro_game_loop = None
        self.jeu = Jeu(self, **kwargs)
        self.running = False
        self.premier_tour = True
        self.retours = Retours()
        self.nb_tours = 120

    def get_jeu_courant(self):
        return self.jeu

    def commande_serveur(self, *args):
        def error(*args):
            return "Commande inconnue"
        if len(args) > 0:
            return getattr(self, "command_" + args[0].lower(), error)(*args[1:])

    def command_start(self, *args):
        self.running = True
        return "Jeu lancé"

    def command_pause(self, *args):
        self.running = False
        return "Jeu en pause"

    def command_nbtours(self, nb):
        self.nb_tours = int(nb)

    def command_list(self, *args):
        donnees_joueurs = {x["id"]:x["score"] for x in self.jeu.univers.get_results()}
        print(donnees_joueurs)
        resultat = {}
        for client in self.clients:
            resultat[client.get_id_joueur()] = str(client.get_id_joueur()) + " " + str(client.get_name()) + " score:" + str(donnees_joueurs.get(client.get_id_joueur(),None))
        return "\n".join(resultat.values())
        #return "\n".join([str(client) for client in self.clients])

    def command_verbosity(self, valeur):
        logging.disable(logging.NOTSET)
        logging.disable(int(valeur))

    def onConnectTCP(self, client_tcp):
        logging.info("Nouveau client TCP connecté")
        jeu = self.get_jeu_courant()
        idJoueur, action = jeu.nouveau_joueur()
        client_tcp.set_id_joueur(idJoueur)
        if not self.premier_tour:
            infos_monde = jeu.toDict()
            infos_monde["idJoueur"] = idJoueur
            client_tcp.send(infos_monde)
            self.retours.ajoute_tous(action)
        else:
            client_tcp.send({"idJoueur":idJoueur})
        self.clients.append(client_tcp)

    async def onConnectWS(self, requestWS):
        logging.info("Nouveau client WebSocket connecté")
        ws = web.WebSocketResponse()
        await ws.prepare(requestWS)
        client = ClientWS(ws, self)
        self.clients.append(client)
        await client.recv()
        return ws

    def quitterJeu(self, client):
        idJoueur = client.get_id_joueur()
        if(idJoueur != "spectateur"):
            self.actions[idJoueur] = '["deconnexion"]'
            self.retours.ajoute_tous(actions.joueur_disconnect(idJoueur))
        self.clients.remove(client)

    def actionClient(self, client, action):
        numjoueur = client.get_id_joueur()
        if numjoueur != "spectateur":
            self.actions[numjoueur] = action
        else:
            logging.error("Pas d'action pour le spectateur !")

    def __repr__(self):
        return " Nombre de clients : " + str(len(self.clients))

    async def on_shutdown(self, app):
        self.coro_game_loop.cancel()
        for client in self.clients:
            await client.close()

    def start_game_loop(self, loop):
        self.coro_game_loop = loop.create_task(self.game_loop())

    def finTour(self):
        self.retours.clear()
        self.actions = {}

    def jouerUnTour(self):
        self.retours.ajoute_spectateur(actions.spectateur_infos(self.jeu.get_infos(
        ) + ([(client.idJoueur, client.name) for client in self.clients if client.idJoueur != "spectateur"],)))
        retours_tour = self.jeu.jouer_un_tour(self.actions)
        self.retours.update(retours_tour)
        for client in self.clients:
            client.send(self.retours.get(client.get_id_joueur()))
        self.finTour()

    async def game_loop(self):
        try:
            while 1:
                await asyncio.sleep(DUREE_TOUR)
                if(self.running and self.nb_tours>0 ):
                    self.nb_tours -= 1
                    if self.premier_tour:
                        self.premier_tour = False
                        jeu = self.get_jeu_courant()
                        infos_monde = jeu.toDict()
                        for client in self.clients:
                            if client.idJoueur != "spectateur":
                                client.send(infos_monde)
                        self.retours.ajoute_spectateur(actions.start())
                    self.jouerUnTour()
        except asyncio.CancelledError:
            logging.info("asyncio CancelledError")
        except Exception as e:
            traceback.print_exc()
        finally:
            logging.info("JEU TERMINE")

    def handleMap(self, request):
        return web.Response(body=self.get_jeu_courant().get_map().get_json().encode(), content_type='text/html')


def handlePrefix(prefixe):
    async def handle(request):
        name = request.match_info.get('name', 'index.html')
        try:
            with open(prefixe + name, 'rb') as index:
                return web.Response(body=index.read(), content_type='text/html')
        except FileNotFoundError:
            logging.error("fichier " + "static/" + name + " introuvable.")
        return web.Response(status=404)
    return handle


async def handleWS(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    while True:
        msg = await ws.receive()
        if msg.tp == web.MsgType.text:
            logging.info("Got message %s" % msg.data)
        elif msg.tp == web.MsgType.close:
            break
    return ws


async def init(loop, **kwargs):
    app = web.Application(loop=loop)
    #app = web.Application()
    controleur = Controleur(**kwargs)
    loop.add_reader(sys.stdin, lambda: print(
        controleur.commande_serveur(*sys.stdin.readline().split())))

    app.router.add_get('/spectateur', controleur.onConnectWS)
    app.router.add_get('/joueur', handleWS)
    app.router.add_get('', handlePrefix("./static/"))
    app.router.add_get('/{name}', handlePrefix("./static/"))
    app.router.add_get('/assets/audio/{name}',
                       handlePrefix("./static/assets/audio/"))
    app.router.add_get(
        '/assets/sprites/{name}', handlePrefix("./static/assets/sprites/"))
    app.router.add_get('/assets/tilemaps/maps/{name}', controleur.handleMap)

    app.router.add_get(
        '/assets/tilemaps/tiles/{name}', handlePrefix("./static/assets/tilemaps/tiles/"))
    # start_background_tasks)
    app.on_startup.append(lambda app: controleur.start_game_loop(app.loop))
    app.on_shutdown.append(controleur.on_shutdown)

    # Serveur TCP
    coro = loop.create_server(lambda: ClientTCP(controleur), '0.0.0.0', 8889)
    server = await coro
    logging.info('Serveur TCP sur : {}'.format(
        server.sockets[0].getsockname()))
    ###

    return app


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Serveur de jeu IUTO', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--map', default="./static/assets/tilemaps/maps/petite_carte_vide.json",
                        help='specifies the mapfile to be used', type=argparse.FileType('r'))

    parser.add_argument('--mortSubite', action="store_true")
    kwargs = vars(parser.parse_args())
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(init(loop, **kwargs))
    print("""
    +++++++++++++++++++++++++++++++++++++++
    Pour visualiser le déroulement du jeu,
    ouvrez un navigateur à l'adresse :
    http://localhost:8081
    +++++++++++++++++++++++++++++++++++++++""")

    print("""
    Les commandes disponibles dans ce terminal :
    start, pause, list, verbosity (0, 20, 30, 40, 50)
    """)

    web.run_app(app, port=8081, print=None)
