import asyncio
import json
import random

"""
Ce client n'envoie pas d'ordres
"""

class EchoClientProtocol(asyncio.Protocol):

    def __init__(self, loop):
        self.loop = loop
        self.ordres = ["hrotate", "trotate"] + ["move"] * 10

    def connection_made(self, transport):
        self.transport = transport
        print("Connecté")
        transport.write(b'{"nickname":"john"}\n')

    def data_received(self, data):
        donnees = json.loads(data.decode())
        print('Data received: {!r}'.format(donnees))

    def connection_lost(self, exc):
        self.loop.stop()

    async def send_message(self, message):
        self.transport.write(json.dumps(message).encode())


loop = asyncio.get_event_loop()

coro = loop.create_connection(lambda: EchoClientProtocol(loop),
                              'localhost', 8889)

loop.run_until_complete(coro)
loop.run_forever()
loop.close()
