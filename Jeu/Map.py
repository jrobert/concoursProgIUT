__author__ = 'julien'
import json
import random


class Map:

    def __init__(self, data_file):
        self.data = json.load(data_file)
        # ajoute les attributs : layers, height, width, tilewidth, properties
        for k, v in self.data.items():
            setattr(self, k, v)

        self.props = {}
        for tileset in self.tilesets:
            if "tileproperties" in tileset:
                for tile in tileset["tileproperties"]:
                    self.props[int(tile) + tileset["firstgid"]
                               ] = tileset["tileproperties"][tile]

        for layer in self.layers:
            self.init_layer(layer)
        self.layers = {layer["name"]: layer for layer in self.layers}

    def init_layer(self, layer):
        print(layer)
        for (indice, numTile) in enumerate(layer["data"]):
            props = self.get_tile_props(numTile)
            if "probability" in props:
                if props["probability"] < random.random():
                    layer["data"][indice] = 0

    def get_tile_props(self, numTile):
        return self.props.get(numTile, {})

    def get_json(self):
        return json.dumps(self.data)

    def get_infos(self):
        return json.dumps(self.layers["Ground"]["data"])

    def random_pos(self):
        """
        :return: une position de la carte prise au hasard
        """
        
        if "Spawn" in self.layers:
            position = (random.randint(0, self.width - 1), random.randint(0, self.height - 1))
            cpt = 0
            while self.get_tile("Spawn", position) == 0 and cpt<10000:
                cpt+=1
                position = (random.randint(0, self.width - 1), random.randint(0, self.height - 1))
            return position

        return (random.randint(0, self.width - 1), random.randint(0, self.height - 1))

    def remove_tile(self, layer, position):
        posx, posy = position
        if self.layers[layer]["data"][posy * self.width + posx] != 0:
            self.layers[layer]["data"][posy * self.width + posx] = 0
            return position
        else:
            return None

    def get_tile(self, layer, position):
        posx, posy = position
        try:
            return self.layers[layer]["data"][posy * self.width + posx]
        except IndexError:
            return 0

    def contient_bonus(self, position):
        return self.get_tile("Bonus", position) != 0

    def supprimer_bonus(self, position):
        tile = self.get_tile("Bonus", position)
        valeur_bonus = self.get_tile_props(tile).get("points", 0)
        self.remove_tile("Bonus", position)
        return valeur_bonus

    def contient_obstacle(self, position):
        return self.get_tile("Ground", position) != 0

    def explosion(self, projectile):
        posProj = projectile.get_position()
        tile = self.get_tile("Ground", posProj)
        if (self.get_tile_props(tile).get("cassable", True)):
            x, y = posProj
            tile = self.remove_tile("Ground", (x, y))
            if tile:
                return (tile,)
            else:
                return ()
        else:
            return ()

    def to_dict_pour_joueurs(self):
        """
        renvoie un dictionnaire représentant la carte.
        (x,y) -> liste des objets. Un objet est un dictionnaire de propriétés
        """
        res = []
        for x in range(self.width):
            for y in range(self.height):
                objet_ground = self.get_tile_props(self.get_tile("Ground", (x, y))).copy()
                if objet_ground:
                    if "probability" in objet_ground:
                        del objet_ground["probability"]
                    objet_ground["pos"]=(x,y)
                    res.append(objet_ground)
                objet_bonus = self.get_tile_props(self.get_tile("Bonus", (x, y))).copy()
                if objet_bonus:
                    if "probability" in objet_bonus:
                        del objet_bonus["probability"]
                    objet_bonus["pos"]=(x,y)
                    res.append(objet_bonus)
        return res
