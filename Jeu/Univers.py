__author__ = 'julien'
from .Map import Map
from .ObjetsJeu import Projectile


class Univers:

    def __init__(self, **kwargs):

        self.tanks = {}  # id -> joueur
        self.projectiles = {}  # idJoueur -> projectile
        self.map = Map(kwargs["map"])

    ##################
    ### Projectiles###
    ##################
    def get_projectiles(self):
        return [p for p in self.projectiles.values() if p]  # self.projectiles

    def remove_projectile(self, id):
        self.projectiles[id] = None

    def new_projectile(self, tank):
        if not (tank.idJoueur in self.projectiles and self.projectiles[tank.idJoueur]):
            self.projectiles[tank.idJoueur] = Projectile(
                tank.get_position(), tank.get_direction(), tank.idJoueur)
            return self.projectiles[tank.idJoueur]
        else:
            raise PlusDeProjectilesException()

    ##################
    ### Tanks      ###
    ##################
    def add_tank(self, tank):
        self.tanks[tank.idJoueur] = tank

    def get_tank(self, id):
        return self.tanks[id]

    def get_tank_from_position(self, position):
        """
        :param position:
        :return: le joueur à la position donnée s'il en est un, None sinon
        """
        for joueur in self.tanks.values():
            if joueur.get_position() == position:
                return joueur

    def remove_tank(self, id):
        """
        :param id: l'identifiant du joueur à supprimer du jeu (deconnexion..)
        :return:
        """
        self.tanks.pop(id)

    ##################
    ### Infos      ###
    ##################
    def get_infos(self):
        return ([joueur.get_infos() for joueur in self.tanks.values()],
                [projectile.get_infos() for projectile in self.projectiles.values() if projectile != None])

    def toDict(self):
        return {"map": self.map.to_dict_pour_joueurs(), "joueurs": [tank.toDict() for tank in self.tanks.values()]}

    def get_results(self):
        return [tank.toDict() for tank in self.tanks.values()]

    def contient_obstacle(self, position):
        return self.get_tank_from_position(position)

    def position_aleatoire(self):
        while(True):
            position = self.map.random_pos()
            if not self.map.contient_obstacle(position) and not self.contient_obstacle(position):
                return position


class PlusDeProjectilesException(Exception):
    pass
