__author__ = 'julien'
import json
from .ObjetsJeu import Tank, Projectile
from .Univers import Univers, PlusDeProjectilesException
from . import actions
import traceback
import logging

ID_PREMIER_JOUEUR = 1


class Jeu:
    def __init__(self, controleur, **kwargs):
        self.mode = kwargs.get("mode", "normal")
        self.univers = Univers(**kwargs)
        self.controleur = controleur
        self.nextId = ID_PREMIER_JOUEUR

    def get_infos(self):
        return self.univers.get_infos() + ([],)

    def toDict(self):
        return self.univers.toDict()

    def get_map(self):
        return self.univers.map

    def remove_tank(self, id):
        """
        :param id: l'identifiant du joueur à supprimer du jeu (deconnexion..)
        :return:
        """
        self.univers.remove_tank(id)

    def jouer_un_tour(self, actions_joueurs):
        """
        :param actions: dictionnaire idJoueur -> action (String)
        :return: dictionnaire idJoueur -> datas (liste des modifs ayant été effectuees)
        """
        #res = {id:[] for id in self.tanks}
        # TODO changer par actions_joueur et vérifier
        res = {id: [] for id in self.univers.tanks}
        res["tous"] = []
        for idJoueur, actions_joueur in actions_joueurs.items():
            retours_publics, retours_prives = self.traiter_actions_joueur(
                actions_joueur, idJoueur)
            if(retours_prives):
                res[idJoueur] += retours_prives
            if(retours_publics):
                res["tous"] += retours_publics
        for projectile in self.univers.get_projectiles():
            for i in range(2):  # les projectiles bougent 2 fois !
                projectile.update()
                dep = actions.projectile_move(projectile.id, projectile.pos)
                if self.univers.map.contient_obstacle(projectile.get_position()):
                    res["tous"] += dep
                    dep = []
                    res["tous"] += self.projectile_explose(projectile, False)
                    break
                tank = self.univers.get_tank_from_position(
                    projectile.get_position())
                if tank and tank.idJoueur != projectile.idJoueur:
                    logging.info("TANK MORT")
                    res["tous"] += dep
                    dep = []
                    res["tous"] += self.projectile_explose(projectile, True)
                    estmort = tank.meurt(self.univers.position_aleatoire())
                    if self.mode == "mort subite" and estmort:
                        res["tous"] += self.action_meurt(tank.idJoueur)
                    else:
                        res["tous"] += actions.joueur_respawn(tank.idJoueur, tank.get_position())
                    self.univers.get_tank(projectile.idJoueur).ajoute_points(10)
                    break
            res["tous"] += dep
        return res

    def projectile_explose(self, projectile, infos):
        """
        fait exploser un projectile et renvoie la liste des modifications que cela engendre.
        :param projectile:
        :return:
        """
        self.univers.remove_projectile(projectile.idJoueur)
        t = actions.projectile_explose(projectile.id, projectile.get_position(
        ), self.univers.map.explosion(projectile), infos)
        return t

    def traiter_actions_joueur(self, actions_joueur, idJoueur):
        """
        :param actions_joueur: liste de chaine sous forme json Chaque chaine est une action possible
        :param idJoueur:
        :return:
        """
        try:
            actions_j = json.loads(actions_joueur)
            verif_actions = actions.verifier_actions(actions_j)
            if verif_actions:
                return (verif_actions, [])

            retours_publics = []
            retours_prives = []
            for action in actions_j:
                retours_publics += getattr(self, "action_" + action,
                                           self.action_error(action))(self.univers.get_tank(idJoueur))
            return (retours_publics, retours_prives)
        except Exception as e:
            return ([],[actions.erreur_info("Le message reçu n'est pas du JSON correct :" + traceback.format_exc())])


    def action_error(self, action):
        def erreur(*args):
            return [actions.erreur_info("Ordre " + action + " inexistant.")]
        return erreur

    def action_move(self, tank):
        if(not self.univers.map.contient_obstacle(tank.get_position_si_deplace())):
            tank.deplace()
            retours = actions.joueur_move(tank.idJoueur)
            if(self.univers.map.contient_bonus(tank.get_position())):
                nb_points = self.univers.map.supprimer_bonus(
                    tank.get_position())
                tank.ajoute_points(nb_points)
                retours += actions.joueur_recupere_bonus(
                    tank.idJoueur, tank.get_position())
            return retours
        else:
            return []

    def action_hrotate(self, tank):
        direction = tank.hrotate()
        return actions.joueur_rotate(tank.idJoueur, direction)

    def action_trotate(self, tank):
        direction = tank.trotate()
        return actions.joueur_rotate(tank.idJoueur, direction)

    def action_shoot(self, tank):
        try:
            p = self.univers.new_projectile(tank)
            return actions.joueur_shoot(p.idJoueur, p.id, p.get_position(), p.get_direction())
        except PlusDeProjectilesException as e:
            return set()

    def action_deconnexion(self, tank):
        self.remove_tank(tank.idJoueur)
        return actions.joueur_disconnect(tank.idJoueur)

    def action_meurt(self, tank):
        self.remove_tank(tank.idJoueur)
        return actions.joueur_die(tank.idJoueur)

    def nouveau_joueur(self):
        """
        :return: l'identifiant d'un nouveau joueur, l'action générée
        """
        position = self.univers.position_aleatoire()
        id = self.nextId
        tank = Tank(id, position)
        self.univers.add_tank(tank)
        self.nextId += 1
        return (id, actions.joueur_connect(id, position, tank.get_direction()))
