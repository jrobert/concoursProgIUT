const TILE_SIZE = 16
_couleurs = [0xB21F35, 0xD92735, 0xFF7435, 0xFFA135, 0xFFCB35, 0xFFF735, 0x00753A, 0x009E47, 0x16DD38, 0x0052A5, 0x0079E7, 0x06A9FC, 0x681E7E, 0x7D3CB5, 0xBD7AF6]

const DureeAnimation = 500

function changeMusic()
{
    music[music.playing].fadeOut(5000);
    music.playing = (music.playing+1) % music.length;
    music[music.playing].fadeIn(5000);
}

function positionne(position){
    return {"x":position[0]*TILE_SIZE+TILE_SIZE/2, "y":position[1]*TILE_SIZE+TILE_SIZE/2}
}

function dirToAngle(direction){
    return Phaser.Math.RAD_TO_DEG*Phaser.Math.angleBetween( 0,0, direction[0], direction[1]);
}

class Joueurs  {
  constructor() {
    this.joueurs = game.add.group();
  }



  connect() //idJoueur,position,direction )
  {
      positionne(...arguments)
  }

  positionne(idJoueur,position,direction)
  {
        let joueur = this.joueurs.getByName("Joueur"+idJoueur);
        if(!joueur)
        {
            let pos = positionne(position);
            joueur = this.joueurs.create(pos.x, pos.y, 'sprites_bs');
            joueur.name="Joueur"+idJoueur;
            joueur.animations.add('right', [14,15], 4, true);
            joueur.tint = _couleurs[idJoueur % _couleurs.length];
            joueur.anchor.setTo(0.5, 0.5);
            joueur.bringToTop();
        }

        joueur.position = positionne(position)
        joueur.direction = direction;
        joueur.angle = dirToAngle(direction);

  }

  move(idJoueur){
        let joueur = this.joueurs.getByName("Joueur"+idJoueur);
        joueur.animations.play('right', 50, true);
        let t = game.add.tween(joueur)
        let destination = { 'x': joueur.x + joueur.direction[0]*TILE_SIZE, 'y':joueur.y+joueur.direction[1]*TILE_SIZE}
        t.to(destination, DureeAnimation, Phaser.Easing.Linear.None, true);
  }

  rotate(idJoueur,direction){
          let joueur = this.joueurs.getByName("Joueur"+idJoueur);
          joueur.direction = direction ;
          let nouvelAngle = dirToAngle(direction);
          if(nouvelAngle-joueur.angle > 180 )
            nouvelAngle-=360;
          game.add.tween(joueur).to( { angle: nouvelAngle  }, DureeAnimation, Phaser.Easing.Linear.None, true);
  }

  stopAnimation(idJoueur){
          let joueur = this.joueurs.getByName("Joueur"+idJoueur);
          joueur.animations.stop();
  }

  shoot(){
     projectiles.add(...arguments);
  }

  recupere_bonus(idJoueur, position){
    map.removeTile(position[0],position[1],"Bonus")
  }
}

class Projectiles  {
  constructor() {
    this.projectiles = game.add.group();
  }
  add(idJoueur, idProjectile, position, direction){
          let projectile = this.projectiles.getByName("Projectile"+idProjectile);
          if(!projectile)
          {
            audio["fire"].play();
            let pos = positionne(position)
            projectile = this.projectiles.create(pos.x,pos.y, 'bullet');
            projectile.anchor.setTo(0.5, 0.5);
            projectile.angle = dirToAngle(direction)+90;
            projectile.name="Projectile"+idProjectile;
            projectile.tint = _couleurs[idJoueur % _couleurs.length];
            projectile.bringToTop();
            projectile.direction = direction;
          }
          else
          {
          projectile.position = positionne(position);
          }
          return projectile;
  }

  explode(idProjectile, position, tilesToRemove, infos)
  {
    setTimeout(()=>
       {
          if(tilesToRemove.length>0){
              audio["explosion"].play();
          }
          if(infos)
              audio["playerdie"].play();
          let projectile = this.projectiles.getByName("Projectile"+idProjectile);
          if(projectile)
          {
              let explosion = this.projectiles.create(projectile.x, projectile.y, 'sprites_bs');
              explosion.anchor.setTo(0.5, 0.5);

              let anim = explosion.animations.add('explose', [167,168,169,170], 4, true);
              explosion.play('explose',10,false);
              anim.onComplete.add( (sprite, animation)=> {this.projectiles.remove(explosion); this.projectiles.remove(projectile); }, this )
          }
          for( let position of tilesToRemove)
          {
             map.removeTile(position[0],position[1],"Ground")
          }
      }, DureeAnimation);
    }

  move(idProjectile, position){
        let projectile = this.projectiles.getByName("Projectile"+idProjectile);
        if(!projectile)
        {
            return;
        // projectile = this.add(idProjectile,position);
        // TODO : il faut que l'info de "à qui appartient quel projectile soit entrée
        // en gérant les projectiles dans la directive "spectateur"
        }
        let pos = positionne(position)
        let t = game.add.tween(projectile)
        t.to(pos, DureeAnimation, Phaser.Easing.Linear.None, true);
  }
}



class scoreZone{

    constructor()
    {
    this.scores = [] // "idJoueur => (Nom, score, text, sprite)
    }


    addJoueur(idJoueur, nom, score)
    {
        if(nom.length > 10) {
            nom = nom.substring(0,10)+".";
        }
        let sprite1 = game.add.sprite(game.world.width + 10  , 23*idJoueur , 'tank');
        sprite1.tint = _couleurs[idJoueur % _couleurs.length];
        sprite1.height = 20;
        sprite1.width=20;
        let style = { font: "16px Courier", fill: "#fff", tabs: [ 120 ] };
        let text1 = game.add.text(game.world.width+36, 23*idJoueur , nom+"\t"+score, style);
        this.scores[idJoueur] = [nom, score,text1, sprite1]
    }

    refresh(idJoueur)
    {
           this.scores[idJoueur][2].text = this.scores[idJoueur][0]+"\t"+this.scores[idJoueur][1] ;
    }
    setName(idJoueur,name)
    {
        if(this.scores[idJoueur])
        {
           this.scores[idJoueur][0] = name;
        }
        else
        {
            this.addJoueur(idJoueur,name,0);
        }
    }

    setScore(idJoueur,score)
    {
        if(this.scores[idJoueur])
        {
           this.scores[idJoueur][1] = score;
        }
        else
        {
            this.addJoueur(idJoueur,"",score);
        }
    }
}

ws = new WebSocket("ws://localhost:8081/spectateur");

function traiterMessage(event){
  var msg = JSON.parse(event.data);
  for( let action of msg)
  {

    if(action[0] == "spectateur")
    {
        let DonneesJoueurs     = action[1][0];
        let DonneesProjectiles = action[1][1];
        let NomsJoueurs        = action[1][3];
        for(let donneesJoueur of DonneesJoueurs)
            {
            let idJoueur = donneesJoueur[0];
            let position = donneesJoueur[1];
            let direction = donneesJoueur[2];
            let score = donneesJoueur[3];
            joueurs.positionne(idJoueur,position,direction);
            scores.setScore(idJoueur,score);
            // TODO : idem pour projectiles
            }
        for(let donneesJoueur of NomsJoueurs)
        {
            if(donneesJoueur[0] != "spectateur")
                    scores.setName(donneesJoueur[0],donneesJoueur[1]);
                    scores.refresh(donneesJoueur[0])

        }
    }

    if(action[0] == "joueur" && ["move","connect","rotate","shoot","recupere_bonus"].indexOf(action[1]) > -1 )
    {
            joueurs[action[1]](...action.slice(2));
    }
    if(action[0] == "projectile" && ["move","explode"].indexOf(action[1])>-1)
    {
            projectiles[action[1]](...action.slice(2))
    }
    if(action[0] == "start")
    {
        let son_start = audio["start"].play();
        son_start.onStop.add(()=>{
            music[music.playing].play();
            setInterval(changeMusic, 200000);
        }, this);
    }


  }
}

var audio = {};
var music=[];
//var game = new Phaser.Game(600, 600, Phaser.AUTO, 'phaser-example', { preload:preload, create: create });
var game = new Phaser.Game(600, 600, Phaser.WEBGL, 'phaser-example', { preload:preload, create: create });

var joueurs;
var projectiles;
var scores;

function initMusic()
{
    music.push( game.add.audio("background1", 1., true))
    music.push( game.add.audio("background2", 1., true))
    music.push( game.add.audio("background3", 1., true))
    music.push( game.add.audio("background4", 1., true))
    music.playing = Math.floor(Math.random() * music.length);
}


function create() {
    audio["fire"] = game.add.audio('fire');
    audio["explosion"] = game.add.audio('explosion', 0.8);
    audio["playerdie"] = game.add.audio('playerdie', 0.8);
    audio["start"] = game.add.audio('start');

    initMusic();

    map = game.add.tilemap('map');

    game.scale.setGameSize(map.tileWidth*map.width+200,map.tileHeight*map.height);
    map.addTilesetImage('Atlas', 'tiles_atlas');

    let layerGround = map.createLayer('Ground');
    layerGround.resizeWorld();

    let layerDecor = map.createLayer('Decor');
    if(layerDecor)
	    layerDecor.resizeWorld();

    let layerBonus = map.createLayer('Bonus');
    layerBonus.resizeWorld();

    layerBonus.visible = false;
    setInterval(function(){
                layerBonus.visible =  true;
                 setTimeout( ()=>{layerBonus.visible=false}, 300);
               }, DureeAnimation);

    joueurs = new Joueurs() ;
    projectiles = new Projectiles();

    scores = new scoreZone();

    game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    game.input.onDown.add(gofull, this);

    //game.stage.disableVisibilityChange = true; //TODO A METTRE EN PHASE DE PROD
    ws.onmessage = traiterMessage;

}



function gofull() {
    if (game.scale.isFullScreen)
    {
        game.scale.stopFullScreen();
    }
    else
    {
        game.scale.startFullScreen(false);
    }
}


function preload() {

    game.load.tilemap('map', 'assets/tilemaps/maps/current_map.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('tiles_atlas', 'assets/tilemaps/tiles/battle_city.png');
    game.load.spritesheet('sprites_bs', 'assets/tilemaps/tiles/battle_city.png', 16, 16);

    game.load.audio('blaster', 'assets/audio/blaster.mp3');
    game.load.audio('fire', 'assets/audio/fire.ogg');
    game.load.audio('explosion', 'assets/audio/explosion.ogg');
    game.load.audio('playerdie', 'assets/audio/gameover.ogg');

    game.load.audio('start', 'assets/audio/gamestart.ogg');
    game.load.audio('background1', 'assets/audio/bu-offensive-birds.mp3');
    game.load.audio('background2', 'assets/audio/Mars.wav');
    game.load.audio('background3', 'assets/audio/Mercury.wav');
    game.load.audio('background4', 'assets/audio/Venus.wav');

    game.load.image('bullet', 'assets/sprites/bullet.png');
    game.load.image('tank', 'assets/sprites/tank.png');

}
